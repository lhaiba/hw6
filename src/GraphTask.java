import java.util.ArrayList;
import java.util.List;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /*
   Koostada meetod, mis teeb kindlaks, kas etteantud sidusas lihtgraafis leidub Euleri tsükkel, ning selle leidumisel
    nummerdab kõik servad vastavalt nende järjekorrale Euleri tsükli läbimisel (vt. ka Fleury' algoritm).
    */

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {

       testCanHandleVolume();
       testNonEuler();
       testTriangle();
       testPentagon();
       testAttachedTriangle();

   }


   /**
    * Vertex represents the tip in the graph.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

       /**
        * method to count Vertex arcs
        * @return number of arcs
        */
       public int numberOfArcs() {
           int degree;
           Arc a = first;
           degree = 0;
           while (a != null && !a.hasBeenOnThatArc) {
               a = a.next;
               degree++;
           }

           return degree;
       }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      private int eulerPathNr;
      private boolean hasBeenOnThatArc = false;

       // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
            return id;
      }
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         int degree;
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            degree = 0;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
               degree++;
            }
            sb.append (" degree ");
            sb.append (degree); //add the degree
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_" + varray [i].toString(),
                       varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_" + varray [vnr].toString(),
                       varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

       /**
        * Finds and prints the Euler path using Fleury algorithm rules
        * @return array of ordered arcs
        */
       public ArrayList<Arc> findEulerPath() {
           if (!hasEulerCycle()) {
               System.out.println("Graph " + id + " doesn't have a Euler cycle");
               return null;
           } else {
               System.out.println("Graph " + id + " has a Euler cycle");
           }

           ArrayList<Arc> eulerPath = new ArrayList<Arc>();
           int eulerPathNr = 1;
           int countOfEdges = getCountOfEdges();
           Vertex startingPoint = this.first;

           while(countOfEdges != 0) {

               Arc arc = startingPoint.first;

               while (arc != null){
                   if(canMoveToNextVertex(arc, countOfEdges)) {
                       arc.eulerPathNr = eulerPathNr;
                       eulerPath.add(arc);
                       //System.out.println("Edge number: " + eulerPathNr + " " + arc.toString());
                       startingPoint.first.hasBeenOnThatArc = true;
                       Arc oppositeArc = getOppositeArc(arc);
                       oppositeArc.hasBeenOnThatArc = true;
                       eulerPathNr++;
                       countOfEdges--;
                       break;
                   }
                   arc = arc.next;
               }
               startingPoint = arc.target;
           }
           return eulerPath;
       }

       /**
        * Takes the array of arc which should form a Euler path and prints it out
        * @param eulerPath
        * @return a string of the Euler path
        */
       public String printEulerPath(ArrayList<Arc> eulerPath) {
           String nl = System.getProperty ("line.separator");
           StringBuffer sb = new StringBuffer (nl);
           if(eulerPath != null){
               for (int i=0; i<eulerPath.size(); i++){
                   Arc arc = eulerPath.get(i);
                   sb.append("Euler path order number:");
                   sb.append(arc.eulerPathNr);
                   sb.append(" for arc: ");
                   sb.append(arc.id);
                   sb.append(nl);
               }
           }
           return sb.toString();
       }

       /**
        * Get an opposite arc to the input
        * @param arc input arv
        * @return oppositeArc
        */
       private Arc getOppositeArc(Arc arc){
           Vertex tempStart = this.first;
           Arc oppositeArc = null;

           String[] parts = arc.id.split("a|_");
           String arcFrom = parts[1];
           String arcTo = parts[2];
           while(tempStart != null) {
               Arc tempArc = tempStart.first;
               while (tempArc != null) {
                   String[] tempParts = tempArc.id.split("a|_");
                   String tempArcFrom = tempParts[1];
                   String tempArcTo = tempParts[2];

                   if (tempArcFrom.equals(arcTo) && tempArcTo.equals(arcFrom)) {
                       oppositeArc = tempArc;
                       return oppositeArc;
                   }
                   tempArc = tempArc.next;
               }
               tempStart = tempStart.next;
           }
           return null;
       }

       /**
        * Method to check whether given arc is valid fo Euler cycle
        * @param arc
        * @param countOfEdges
        * @return boolean
        */
       private boolean canMoveToNextVertex(Arc arc, int countOfEdges) {
           if(arc.target.next == null) {
               if(!arc.hasBeenOnThatArc) {
                   return true;
               } else {
                   return false;
               }
           }
               int numberOfArcs = arc.target.next.numberOfArcs();

           if ((numberOfArcs > 1 && !arc.hasBeenOnThatArc)
                   || (numberOfArcs == 0 && countOfEdges <= 2 && !arc.hasBeenOnThatArc)){
               return true;
           } else {
               return false;
           }
       }

       /**
        * method to count the number of edges of the graph
        * @return number of edges
        */
       public int getCountOfEdges() {
           int degree = 0;
           Vertex v = first;
           while (v != null) {
               Arc a = v.first;
               while (a != null) {
                   a = a.next;
                   degree++;
               }
               v = v.next;
           }
           return degree / 2;
       }


      /**
       * Finds if a graph has a Euler cycle by checking the vertex degrees
       * if all vertex degrees are even, then Euler cycle can be found
       * @return whether Euler cycle can be found in graph
       */
      public boolean hasEulerCycle(){
         boolean b = false;
         int degree = 0;
         //count vertex degrees
         Vertex v = first;
         while (v != null) {
            Arc a = v.first;
            degree = 0;
            while (a != null) {
               a = a.next;
               degree++;
            }

          // check if there are odd vertex degrees
            if((degree % 2) == 0){
               b = true;
            } else {
               b = false;
               break;
            }
            v = v.next;
         }
         return b;
      }
   }


    /**
     * Test to check that graph has no Euler cycle in it
     */
    public void testNonEuler() {

        Graph g1 = new Graph("NonEuler");
        Vertex vertex1 = g1.createVertex("v1");
        Vertex vertex2 = g1.createVertex("v2");
        Vertex vertex3 = g1.createVertex("v3");

        g1.createArc("a" + vertex1.toString() + "_" + vertex2.toString(), vertex1, vertex2);
        g1.createArc("a" + vertex2.toString() + "_" + vertex1.toString(), vertex2, vertex1);

        g1.createArc("a" + vertex1.toString() + "_" + vertex3.toString(), vertex1, vertex3);
        g1.createArc("a" + vertex3.toString() + "_" + vertex1.toString(), vertex3, vertex1);

        System.out.println(g1);
        System.out.println ( g1.printEulerPath(g1.findEulerPath()));
    }

    /**
     * test to check if solution can handle larger volumes
     */
    public void testCanHandleVolume(){

        Graph g = new Graph ("G");
        g.createRandomSimpleGraph (50, 50);
        System.out.println (g);
        g.findEulerPath();
    }

    /**
     * test to check if solution returns correct Euler cycle on a simple graph
     */
   public void testTriangle(){

       Graph g1 = new Graph ("Triangle");
       Vertex vertex1 = g1.createVertex("v1");
       Vertex vertex2 = g1.createVertex("v2");
       Vertex vertex3 = g1.createVertex("v3");

       g1.createArc("a" + vertex1.toString() + "_" + vertex2.toString(), vertex1, vertex2);
       g1.createArc("a" + vertex2.toString() + "_" + vertex1.toString(), vertex2, vertex1);

       g1.createArc("a" + vertex1.toString() + "_" + vertex3.toString(), vertex1, vertex3);
       g1.createArc("a" + vertex3.toString() + "_" + vertex1.toString(), vertex3, vertex1);

       g1.createArc("a" + vertex2.toString() + "_" + vertex3.toString(), vertex2, vertex3);
       g1.createArc("a" + vertex3.toString() + "_" + vertex2.toString(), vertex3, vertex2);
       System.out.println (g1);
       System.out.println ( g1.printEulerPath(g1.findEulerPath()));

   }

    /**
     * test to check a larger graph
     */
   public void testPentagon(){
       Graph g2 = new Graph ("Pentagon");

       Vertex vertex4 = g2.createVertex("v4");
       Vertex vertex5 = g2.createVertex("v5");
       Vertex vertex6 = g2.createVertex("v6");
       Vertex vertex7 = g2.createVertex("v7");
       Vertex vertex8 = g2.createVertex("v8");

       g2.createArc("a" + vertex4.toString() + "_" + vertex5.toString(), vertex4, vertex5);
       g2.createArc("a" + vertex5.toString() + "_" + vertex4.toString(), vertex5, vertex4);

       g2.createArc("a" + vertex4.toString() + "_" + vertex8.toString(), vertex4, vertex8);
       g2.createArc("a" + vertex8.toString() + "_" + vertex4.toString(), vertex8, vertex4);

       g2.createArc("a" + vertex5.toString() + "_" + vertex6.toString(), vertex5, vertex6);
       g2.createArc("a" + vertex6.toString() + "_" + vertex5.toString(), vertex6, vertex5);

       g2.createArc("a" + vertex6.toString() + "_" + vertex7.toString(), vertex6, vertex7);
       g2.createArc("a" + vertex7.toString() + "_" + vertex6.toString(), vertex7, vertex6);

       g2.createArc("a" + vertex8.toString() + "_" + vertex7.toString(), vertex8, vertex7);
       g2.createArc("a" + vertex7.toString() + "_" + vertex8.toString(), vertex7, vertex8);

       System.out.println (g2);
       System.out.println ( g2.printEulerPath(g2.findEulerPath()));
   }

    /**
     * test to check if solution finds correct Euler cycle where one vertex has more than 2 arcs
     */
    public void testAttachedTriangle(){
        Graph g1 = new Graph ("Attached Triangles");

        Vertex vertex1 = g1.createVertex("v1");
        Vertex vertex2 = g1.createVertex("v2");
        Vertex vertex3 = g1.createVertex("v3");
        Vertex vertex4 = g1.createVertex("v4");
        Vertex vertex5 = g1.createVertex("v5");

        g1.createArc("a" + vertex1.toString() + "_" + vertex2.toString(), vertex1, vertex2);
        g1.createArc("a" + vertex2.toString() + "_" + vertex1.toString(), vertex2, vertex1);

        g1.createArc("a" + vertex1.toString() + "_" + vertex3.toString(), vertex1, vertex3);
        g1.createArc("a" + vertex3.toString() + "_" + vertex1.toString(), vertex3, vertex1);

        g1.createArc("a" + vertex2.toString() + "_" + vertex3.toString(), vertex2, vertex3);
        g1.createArc("a" + vertex3.toString() + "_" + vertex2.toString(), vertex3, vertex2);

        g1.createArc("a" + vertex4.toString() + "_" + vertex3.toString(), vertex4, vertex3);
        g1.createArc("a" + vertex3.toString() + "_" + vertex4.toString(), vertex3, vertex4);

        g1.createArc("a" + vertex5.toString() + "_" + vertex3.toString(), vertex5, vertex3);
        g1.createArc("a" + vertex3.toString() + "_" + vertex5.toString(), vertex3, vertex5);

        g1.createArc("a" + vertex5.toString() + "_" + vertex4.toString(), vertex5, vertex4);
        g1.createArc("a" + vertex4.toString() + "_" + vertex5.toString(), vertex4, vertex5);

        System.out.println (g1);
        System.out.println ( g1.printEulerPath(g1.findEulerPath()));
    }
}

